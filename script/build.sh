#!/bin/sh
clear
echo "Building psyn Project Manager"
[ -d build/psyn ] && echo "Folder Exists!" || mkdir -p build/psyn
deno bundle src/psyn_project.ts > build/psyn/project.js
echo "Done"