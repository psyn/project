import { psynEnv } from "https://gitlab.com/psyn/environment/-/raw/master/src/psyn_environment.ts";

// 🏴🏴‍☠️✅❕❗️🟦🟥🟩🟨💽🛠🗄🗳🗂📁🗃📦⏰⏱🕰⏳

export interface command {
    name:       string,
    method:     string,
    parameters: object,
    flags:      Array<string>,
    source:     psynProject,
    prototype:  any
}

export class psynProject {

    private encoder: TextEncoder = new TextEncoder();
    private decoder: TextDecoder = new TextDecoder();

    constructor() {

        if (!Deno.args[0]) {
            this.main();
            return;
        }

        const cmd: command = this.command(Deno.args[0]);
        this.executeCommand(cmd);
    }

    get self() : any {return this as any;}

    private methodExists(method: string) : boolean {
        return (this.self[method] ? true : false);
    }
    
    private async main() {

        const displayHeader = () => {
            psynEnv.announceStatus('');
            psynEnv.announceStatus("🗂  psyn Project Manager\n🏴 ‍version 0.1.0\n");
            psynEnv.announceStatus("\t[Command]\t[Description]");
        };

        

        displayHeader();
        this.getCommandChoce();
        psynEnv.announceStatus("");

    }

    private mainMenu() : Object {
        return {
            'build':    ["🛠\tBuild", 'Bundle project files to JavaScript'],
            'init':     ["📦\tInit", 'Create a new Application or Module'],
            'install':  ["🗄\tInstall", 'Install your project to the local machine'],
            'quit':     ["❕\tQuit", "Quit psyn Project Manager"]
        };
    }

    private async getProjectName(): Promise<string> {
        return '';
    }

    private async getProjectType(): Promise<string> {
        return '';
    }

    private async getCommandChoce(): Promise<string> {

        const displayMenu = () => {
            const menu: object = this.mainMenu();

            for (let cmdName in menu) {
                const menuSet: any          = menu as any;
                const item: Array<string>   = menuSet[cmdName];

                psynEnv.announceStatus(`${item[0]}\t\t${item[1]}`);
            }

        };

        displayMenu();
        return '';
    }

    private async createEnvFolders() {

        const requiredFolders: Array<string> = [
            'build',
            'src',
            'script'
        ];

    }

    private async createEnvFiles(projectName: string): Promise<boolean> {

        const requiredFiles: Array<string> = [
            '.gitignore',
            'README.md',
            'script/build.sh',
            `src/${projectName}.ts`
        ];

        return false;
    }

    private async createNewApplication() {

    }

    private async createNewModule() {
        
    }

    private command(methodName: string) : command {

        const cliArgs: Array<string>            = Deno.args;
        const parameters: {[key: string]: any } = {};
        const flags: Array<string>              = [];

        for (let i in cliArgs) {

            const arg = cliArgs[i];

            if (arg.includes('=')) {

                const parts         = arg.split('=', 2);

                let key: string     = parts[0] ? parts[0] : '';
                let val: any        = parts[1] ? parts[1] : '';

                if (!key) continue;

                if (key.startsWith('--')) {
                    key = key.substr(2);
                } else if (key.startsWith('-')) {
                    key = key.substr(1);
                }

                if (!val && val !== 0 && val !== false) val = false;

                parameters[key]     = val;
                continue;

            } else if (arg.startsWith('--')) {

                const flag = arg.substr(2);
                flags.push(flag);

            } else if (arg.startsWith('-')) {

                const flag = arg.substr(1);
                flags.push(flag);

            }
        }

        return {
            'name':         methodName,
            'method':       `cmd_${methodName}`,
            'parameters':   parameters,
            'flags':        flags,
            'source':       this,
            'prototype':    this.self
        };
    }

    public async executeCommand(cmd: command) : Promise<any> {

        if (this.methodExists(cmd.method) == false) {
            psynEnv.announceError(`Command '${cmd.name}' not understood`);
            return;
        }

        const handler: Function  = this.self[cmd.method];
        const result: any        = await handler(cmd.parameters, cmd.flags, cmd.source);

        return result;
    }

    private async cmd_init(param: object, flags: Array<string>, source: psynProject) {
        psynEnv.announceStatus("psyn 📦 Initialize");
    }

    private async cmd_build(param: object, flags: Array<string>, source: psynProject) {
        psynEnv.announceStatus("Deno 🛠  Build");
    }

    private async cmd_install(param: object, flags: Array<string>, source: psynProject) {
        psynEnv.announceStatus("Deno 🗄  Install");
    }

    private cmd_quit() {
        psynEnv.announceStatus("Goodbye");
    }

}

const pm = new psynProject();